define([
    'ko',
    'uiComponent',
    'mage/url',
    'mage/storage',
], function (ko, Component, urlBuilder,storage) {
    'use strict';
    var id=32;

    return Component.extend({

        defaults: {
            template: 'Magenest_KnockoutJs/test',
        },

        productList: ko.observableArray([]),

        getProduct: function () {
            var self = this;
            var serviceUrl = urlBuilder.build('knockout/test/product?id='+id);
            id ++;
            function getProductError() {
                alert('Invalid product ID');
            }
            return storage.post(
                serviceUrl,
                ''
            )
                .done(
                function (response) {
                    if (response == 'invalid'){
                        getProductError();
                    } else {
                        self.productList.push(JSON.parse(response));
                    }
                }
            )
                .fail(
                    function (responsive) {
                        getProductError();
                    }
                );
        },

    });
});