<?php
namespace Magenest\Modal\Controller\Modal;

use Magenest\Modal\Controller\Modal;

class Index extends Modal
{
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Modal Test Page'));
        return $resultPage;
        // TODO: Implement execute() method.
    }
}